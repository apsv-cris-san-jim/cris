package es.upm.dit.apsv.cris.servlets;

public class URLHelper {
	
	private static URLHelper uh;
	private String restUrl;
	
	public static URLHelper getInstance() {
		if(uh == null) {
			uh = new URLHelper();
		}
		return uh;
	}
	
	public URLHelper() {
		String envValue = System.getenv("CRISSERVICE_SRV_SERVICE_HOST");
		if(envValue == null) //not in Kubernetes
			restUrl = "http://localhost:8080/CRISSERVICE";
		else //In k8, DNS service resolution in Kubernetes
			restUrl = "http://crisservice-srv/CRISSERVICE";
		}
	
	public String getCrisURL() {
		return restUrl;
	}

}
