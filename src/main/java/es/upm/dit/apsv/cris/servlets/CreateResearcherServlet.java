package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Researcher;

@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 getServletContext().getRequestDispatcher("/AdminView.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		Researcher r = (Researcher) req.getSession().getAttribute("user");
		if ((r != null) && (r.getId().contentEquals("root"))) {
			
			Researcher researcher = new Researcher();
			researcher.setId(req.getParameter("id"));
			researcher.setName(req.getParameter("name"));
			researcher.setLastname(req.getParameter("lastname"));
			researcher.setEmail(req.getParameter("email"));
				                
	        Client client = ClientBuilder.newClient(new ClientConfig());
	        Response response  = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers")
					.request().post(Entity.entity(researcher, MediaType.APPLICATION_JSON), Response.class);
	        
	        req.getSession().setAttribute("created", "New researcher created correctly");
			res.sendRedirect(req.getContextPath() + "/AdminServlet");

		} else {
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(req, res);
	        req.getSession().setAttribute("message", "New researcher not created correctly");
		}
		
	}

}
