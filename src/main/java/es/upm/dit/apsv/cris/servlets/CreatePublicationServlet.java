package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 getServletContext().getRequestDispatcher("/ResearcherView.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
			
			Publication publication = new Publication();
			publication.setId(req.getParameter("id"));
			publication.setTitle(req.getParameter("title"));
			publication.setPublicationName(req.getParameter("publicationName"));
			publication.setPublicationDate(req.getParameter("publicationDate"));
			publication.setCiteCount(Integer.valueOf(req.getParameter("citeCount")));
			publication.setAuthors(req.getParameter("authors"));
				                
	        Client client = ClientBuilder.newClient(new ClientConfig());
	        Response response  = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications")
					.request().post(Entity.entity(publication, MediaType.APPLICATION_JSON), Response.class);
	        
	        req.getSession().setAttribute("create_publication", "New publication created correctly");
	        Researcher r = (Researcher) req.getSession().getAttribute("user");
	    	res.sendRedirect(req.getContextPath() + "/ResearcherServlet" + "?id=" + r.getId());
		
	}

}
