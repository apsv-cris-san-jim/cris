package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;



/*
 * <c:forEach items="${pi.authors.split(\";\"}" var="ri">
			<a href="ResearcherServlet?id=${ri.id}"> ${ri.id} </a> ,
			</c:forEach>
 */
@WebServlet("/PublicationServlet")
public class PublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = (String) request.getParameter("id");
		Client client = ClientBuilder.newClient(new ClientConfig());
		
		Publication pi = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/" + id)
		        .request().accept(MediaType.APPLICATION_JSON).get(Publication.class);        
		request.setAttribute("pi", pi);
		
		getServletContext().getRequestDispatcher("/PublicationView.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
