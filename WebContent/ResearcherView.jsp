<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>   
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Researcher</title>
<%@ include file = "Header.jsp"%>
</head>
<body>

<table>
<tr>
<th>Id</th><th>Name</th><th>Last name</th><th>URL</th><th>Email</th>
</tr>
	<tr>
			<td> ${ri.id} </a></td>
            <td>${ri.name}</td>
            <td>${ri.lastname}</td>
			<td> <a href="${ri.scopusURL}">Scopus URL</a></td>
			<td>${ri.email}</td>
	</tr>
</table>

<c:if test="${ri.id == user.id}">
<p style="color: red;">${create_publication}</p>
<h2> Create Publication </h2>
<form action="CreatePublicationServlet" method="post">
        <input type="text" name="id" placeholder="Publication Id">
        <input type="text" name="title" placeholder="Title">
        <input type="text" name="publicationName" placeholder="Publication name">
        <input type="text" name="publicationDate" placeholder="Publication date">
        <input type="text" name="authors" placeholder="Authors">
        <input type="text" name="citeCount" placeholder="citeCount">
        <button type="submit">Create publication</button>
</form>


</c:if>


<h2> Publications </h2>
<table>
<c:forEach items="${publications}" var="pi">
        <tr>
		<td> <a href="PublicationServlet?id=${pi.id}"> ${pi.id}</a></td>
        </tr>
</c:forEach>
</table>

</body>
</html>